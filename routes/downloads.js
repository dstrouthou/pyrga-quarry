var express = require('express');
var async = require("async");
var router = express.Router();
var blobUriCert = 'https://' + 'latomiopirgadocs' + '.blob.core.windows.net';

const azure = require('azure-storage');
const blobServiceCert = azure.createBlobService('latomiopirgadocs',
    'AtFHqvj0SPMeaS5MZnnGtA7ZQGp1N2SRv7ZlqN4P27zTrEuuI2Bz9iPHg066GSKDVPOjOVQUEG5FfRjQH9UJkg==', blobUriCert);

router.get('/', function(req, res, next) {
    var myreports = {};

    async.series([
        function (callback) {
            blobServiceCert.listBlobsSegmented('pirgacertificates', null, function (error, results) {
                if (error) {
                    console.log(error);
                } else {
                    // for (var i = 0, container; container = results.entries[i]; i++) {
                    //     console.log("hello" + container);
                    // }

                    myreports.reportscert = results;
                    callback(null, myreports);
                }
            });
        },
        function (callback) {
            blobServiceCert.listBlobsSegmented('pirgadop', null, function (error, results) {
                if (error) {
                    console.log(error);
                } else {
                    // for (var i = 0, container; container = results.entries[i]; i++) {
                    //     console.log("demetris" + container);
                    // }

                    myreports.reportsdop = results;
                    callback(null, myreports);
                }
            });
        },
        function (callback) {

            blobServiceCert.listBlobsSegmented('pirgachemical', null, function (error, results) {
                if (error) {
                    console.log(error);
                } else {
                    // for (var i = 0, container; container = results.entries[i]; i++) {
                    //     console.log("demetris" + container);
                    // }

                    myreports.reportschem = results;
                    callback(null, myreports);
                }
            });

        }
    ], function (err, results) {

        res.render('downloads', {
            title: 'Express',
            reportscert: myreports.reportscert,
            reportsdop: myreports.reportsdop,
            reportschem: myreports.reportschem
        });
    });
});

module.exports = router;
